﻿using System;
using System.Windows.Data;

namespace AIStudio.Wpf.DiagramDesigner
{

    public class ArrowPathConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is PathStyle arrowStyle)
            {
                return ArrowPathData.Arrow[arrowStyle];
            }
            else if (value is string)
            {
                return value;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
