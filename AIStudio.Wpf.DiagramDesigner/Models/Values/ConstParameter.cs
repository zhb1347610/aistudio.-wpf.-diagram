﻿using System;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConstParameter : BindableBase, IParameter
    {
        public ConstParameter()
        {
        }

        public ConstParameter(ConstParameterItem item) 
        {
            Text = item.Text;
            Value = item.Value?.ToString();
        }

        private string _text;

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                SetProperty(ref _text, value);
            }
        }

        private object _value;

        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                SetProperty(ref _value, value);
            }
        }



        public void Add(object value)
        {
            if (double.TryParse(Value?.ToString() ?? "", out var value1) && double.TryParse(value?.ToString() ?? "", out var value2))
            {
                Value = value1 + value2;
            }
            else
            {
                Value = $"{Value}{value}";
            }
        }

        public T GetValue<T>()
        {
            T var1 = default(T);

            if (Value is T double1)
            {
                var1 = double1;
            }
            else
            {
                try
                {
                    var1 = (T)Convert.ChangeType(Value?.ToString(), typeof(T));
                }
                catch { }
            }

            return var1;

        }

        public override string ToString()
        {
            return $"{Text}:{Value}";
        }
    }
}
