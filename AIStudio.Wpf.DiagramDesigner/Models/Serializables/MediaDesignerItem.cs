﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class MediaDesignerItem : DesignerItemBase
    {
        public MediaDesignerItem()
        {

        }

        public MediaDesignerItem(GifImageItemViewModel item) : base(item)
        {
    
        }

        public MediaDesignerItem(MediaItemViewModel item) : base(item)
        {

        }

    }
}
