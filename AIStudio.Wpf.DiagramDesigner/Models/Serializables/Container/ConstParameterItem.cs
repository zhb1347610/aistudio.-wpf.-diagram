﻿using AIStudio.Wpf.DiagramDesigner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    [XmlInclude(typeof(ConstParameterItem))]
    public class ConstParameterItem
    {
        [XmlAttribute]
        public string Text { get; set; }

        [XmlAttribute]
        public virtual string Value { get; set; }     

        public ConstParameterItem()
        {

        }

        public ConstParameterItem(ConstParameter viewmodel) 
        {
            Text = viewmodel?.Text;
            Value = viewmodel?.Value?.ToString();
        }
    }
}
