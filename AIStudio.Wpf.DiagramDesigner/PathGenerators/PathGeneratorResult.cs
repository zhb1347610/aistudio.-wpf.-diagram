﻿using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class PathGeneratorResult
    {
        public PathGeneratorResult(string[] paths, double sourceMarkerAngle, PointBase sourceMarkerPosition,
            double targetMarkerAngle, PointBase targetMarkerPosition)
        {
            Paths = paths;
            SourceMarkerAngle = sourceMarkerAngle;
            SourceMarkerPosition = sourceMarkerPosition;
            TargetMarkerAngle = targetMarkerAngle;
            TargetMarkerPosition = targetMarkerPosition;
        }

        public string[] Paths
        {
            get;
        }
        public double SourceMarkerAngle
        {
            get;
        }

        /// <summary>
        /// SourceMarker左上角的点
        /// </summary>
        public PointBase SourceMarkerPosition
        {
            get;
        }
        public double TargetMarkerAngle
        {
            get;
        }
        /// <summary>
        /// TargetMarker左上角的点
        /// </summary>
        public PointBase TargetMarkerPosition
        {
            get;
        }
    }
}
