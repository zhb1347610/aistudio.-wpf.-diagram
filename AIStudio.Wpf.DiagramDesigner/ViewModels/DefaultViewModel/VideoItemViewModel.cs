﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class VideoItemViewModel : MediaItemViewModel
    {
        protected override string Filter { get; set; } = "视频|*.wmv;*.asf;*.asx;*.rm;*.rmvb;*.mp4;*.3gp;*.mov;*.m4v;*.avi;*.dat;*.mkv;*.flv;*.vob";

        public VideoItemViewModel() : this(null)
        {
        }

        public VideoItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public VideoItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public VideoItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new MediaDesignerItem(this);
        }
    }
}
