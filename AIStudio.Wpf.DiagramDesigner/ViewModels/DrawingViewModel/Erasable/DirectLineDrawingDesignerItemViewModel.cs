﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DirectLineDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public DirectLineDrawingDesignerItemViewModel()
        {
        }

        public DirectLineDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasableDirectLine, startPoint, colorViewModel, erasable)
        {
        }

        public DirectLineDrawingDesignerItemViewModel(IDiagramViewModel root, List<Point> points, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.DirectLine, points, colorViewModel, erasable)
        {

        }

        public DirectLineDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public DirectLineDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void InitNewDrawing()
        {
            if (IsFinish)
            {
                var geometry = new PathGeometry();
                var figure = new PathFigure { StartPoint = Points[0] };
                geometry.Figures.Add(figure);

                for (int i = 1; i < Points.Count; i++)
                {
                    LineSegment arc = new LineSegment(Points[i], true);
                    geometry.Figures[0].Segments.Add(arc);
                }

                Geometry = geometry;
            }
            base.InitNewDrawing();
        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            var point = e.GetPosition(sender);
            if (Points == null || Points.Count == 0)//没有起始点
            {
                return true;
            }

            if ((Points.LastOrDefault() - point).Length < ColorViewModel.LineWidth)
            {
                return true;
            }

            //按着LeftShift自动封闭曲线
            if (Keyboard.IsKeyDown(Key.LeftShift) == true)
            {
                point = Points[0];
            }

            if (Geometry is PathGeometry geometry)
            {

            }
            else
            {
                geometry = new PathGeometry();
                var figure = new PathFigure { StartPoint = Points[0] };
                geometry.Figures.Add(figure);
            }

            LineSegment arc = new LineSegment(point, true);

            var preLine = geometry.Figures[0].Segments.LastOrDefault();
            if (preLine != _lastLine)
            {
                geometry.Figures[0].Segments.Remove(preLine);
            }
            geometry.Figures[0].Segments.Add(arc);
            Geometry = geometry;

            return true;
        }

        private LineSegment _lastLine;
        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            //左键添加点
            if (e.ChangedButton == MouseButton.Left)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points.LastOrDefault() - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }
                //按着LeftShift自动封闭曲线
                if (Keyboard.IsKeyDown(Key.LeftShift) == true)
                {
                    point = Points[0];
                }
                Points.Add(point);

                if (Geometry is PathGeometry geometry)
                {

                }
                else
                {
                    geometry = new PathGeometry();
                    var figure = new PathFigure { StartPoint = Points[0] };
                    geometry.Figures.Add(figure);
                }

                var preLine = geometry.Figures[0].Segments.LastOrDefault();
                if (preLine != _lastLine)
                {
                    geometry.Figures[0].Segments.Remove(preLine);
                }

                _lastLine = new LineSegment(point, true);
                geometry.Figures[0].Segments.Add(_lastLine);
                Geometry = geometry;

                IsFinish = true;
            }
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            if (e != null && e.ChangedButton == MouseButton.Left && Keyboard.IsKeyDown(Key.LeftShift) == false)//按着LeftShift自动封闭曲线
            {
                return false;
            }
            else
            {
                if (Geometry is PathGeometry geometry)
                {
                    var preLine = geometry.Figures[0].Segments.LastOrDefault();
                    if (preLine != _lastLine && _lastLine != null)
                    {
                        geometry.Figures[0].Segments.Remove(preLine);
                    }

                    _lastLine = null;
                    Geometry = geometry;
                }
                return base.OnMouseUp(sender, e);
            }
        }
    }
}
