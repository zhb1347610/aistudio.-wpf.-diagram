﻿using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Geometrys
{
    public delegate IShape ShapeDefiner(DesignerItemViewModelBase node);

    public static class Shapes
    {
        public static IShape Rectangle(DesignerItemViewModelBase node) => new RectangleBase(node.Position, node.Size);

        public static IShape Circle(DesignerItemViewModelBase node)
        {
            var halfWidth = node.Size.Width / 2;
            var centerX = node.Position.X + halfWidth;
            var centerY = node.Position.Y + node.Size.Height / 2;
            return new EllipseBase(centerX, centerY, halfWidth, halfWidth);
        }

        public static IShape Ellipse(DesignerItemViewModelBase node)
        {
            var halfWidth = node.Size.Width / 2;
            var halfHeight = node.Size.Height / 2;
            var centerX = node.Position.X + halfWidth;
            var centerY = node.Position.Y + halfHeight;
            return new EllipseBase(centerX, centerY, halfWidth, halfHeight);
        }
    }
}
