﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Controls;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.Flowchart.ViewModels
{
    public class MiddleFlowNode : FlowNode
    {
        public MiddleFlowNode() : this(null)
        {

        }

        public MiddleFlowNode(IDiagramViewModel root) : base(root, NodeKinds.Middle)
        {

        }

        public MiddleFlowNode(IDiagramViewModel root, DesignerItemBase designer) : base(root, designer)
        {

        }

        public MiddleFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        private List<string> _userIds = new List<string>();
        [Browsable(true)]
        [StyleName("UserIdsStyle")]
        public List<string> UserIds
        {
            get
            {
                return _userIds;
            }
            set
            {
                SetProperty(ref _userIds, value);
            }
        }

        private List<string> _roleIds = new List<string>();
        [Browsable(true)]
        [StyleName("RoleIdsStyle")]
        public List<string> RoleIds
        {
            get
            {
                return _roleIds;
            }
            set
            {
                SetProperty(ref _roleIds, value);
            }
        }

        private string _actType;
        [Browsable(true)]
        [StyleName("ActTypeStyle")]
        public string ActType
        {
            get
            {
                return _actType;
            }
            set
            {
                SetProperty(ref _actType, value);
            }
        }

        #region 模拟使用
        public bool SimulateApprove
        {
            get;set;
        }
        #endregion

        protected override void ExecuteEditCommand(object param)
        {
            if (IsReadOnly == true) return;

            if (SimulateApprove)
            {
                if (Status == 1)
                {
                    MiddleFlowNodeData data = new MiddleFlowNodeData();
                    if (visualiserService.ShowDialog(data) == true)
                    {
                        FlowchartService.Approve(this, data.Status, data.Remark);
                    }
                }
                else
                {
                    MessageBox.Show("该节点不能进行审批！！！");
                }
            }
        }

        public override Dictionary<string, string> PropertiesSetting
        {
            get
            {
                return new Dictionary<string, string>()
                { 
                    { "Name","名称" },
                    { "Text","文本" },
                    {"UserIds", "用户" },
                    {"RoleIds", "角色" },
                    {"ActType", "or/and" }
                };
            }
        }
    }
}
